<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  @include('sweetalert::alert')
    @if (session()->has('user'))
    <nav class="navbar navbar-expand-lg bg-body-tertiary" style="margin: 5px; margin-right: 30px">
        <div class="container-fluid">
          <a class="navbar-brand" href="/dashboard">Ecommerce Dashboard</a>
          <ul class="nav justify-content-end">
            <li class="nav-item">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ session('user') }}
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end">
                      <li><a class="dropdown-item" href="/logout">Logout</a></li>
                    </ul>
                  </div>
            </li>
          </ul>
        </div>
      </nav>
      <div class="card text-left" style="margin-left:250px; margin-right:250px">
        <div class="card-header">
          Edit Product
        </div>
        <div class="card-body">
          {{-- {{session('product')[0]}} --}}
            <form method="post" action="/product/edit/{{session('product')[0]->id}}" enctype="multipart/form-data">
                @csrf
                <img src="{{ asset('storage/images/'.session('product')[0]->image)}}" alt="productimg" width="100px">

                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Change Image</label>
                  <input type="file" name="photo"  class="form-control" accept=".jpg,.jpeg,.bmp,.png,.gif,.doc,.docx,.csv,.rtf,.xlsx,.xls,.txt,.pdf,.zip">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Title</label>
                  <input type="text" name="title" value={{session('product')[0]->title}} class="form-control" id="exampleInputPassword1">
                </div>
                <label for="exampleInputPassword1" class="form-label">Description</label>
                <div class="form-floating">
                    <textarea class="form-control" name="description" id="floatingTextarea2" style="height: 100px">{{session('product')[0]->description}}</textarea>
                  </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Quantity</label>
                    <input type="text" name="quantity" value={{session('product')[0]->quantity}} class="form-control" id="exampleInputPassword1">
                    <div  class="form-text" style="font-size: 15px">
                      </div> 
                </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Price</label>
                    <input type="text" name="price"  value={{session('product')[0]->price}} class="form-control" id="exampleInputPassword1">
                    {{-- <div  class="form-text" style="font-size: 15px"> --}}
                      {{-- </div>   --}}
                </div>
        </div>
        <div class="card-footer text-muted" style="display: flex; justify-content: space-between">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-primary" href="/dashboard">Cancel</a>
        </div>
      </div>
     
    @endif
     
</html>