<?php

use App\Http\Controllers\ProductController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/dashboard', function () {
    $products=Product::all();
    session(['product'=>$products]);
    return view('dashboard');
});
Route::post('/dashboard/product',[ProductController::class,'storeproduct']);
Route::post('/register', [UserController::class,'register']);
Route::post('/login', [UserController::class,'login']);
Route::get('/logout', [UserController::class,'logout']);
Route::get('/product/edit/{id}',[ProductController::class,'editproductview']);
Route::post('/product/edit/{id}',[ProductController::class,'updateproduct']);
Route::get('/product/delete/{id}',[ProductController::class,'deleteproduct']);


