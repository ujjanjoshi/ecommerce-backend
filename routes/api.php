<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/register', [UserController::class,'register']);
Route::post('/login', [UserController::class,'logincustomer']);
Route::get('/product', [ProductController::class,'viewallproduct']);
Route::get('/product/{id}', [ProductController::class,'viewproduct']);
Route::post('/cartproducttemp', [ProductController::class,'cartproducttemporary']);
Route::get('/getimage/{path}', [ProductController::class,'getImage']);


Route::get('/addcart/{id}/{cid}', [CartController::class,'create']);
Route::get('/deletecart/{id}',[CartController::class,'destroy']);
Route::get('/viewcart',[CartController::class,'index']);
Route::get('/decreasequantity/{id}/{quanity}',[CartController::class,'decreasequantity']);
Route::get('/increasequantity/{id}/{quantity}',[CartController::class,'increasequantity']);


