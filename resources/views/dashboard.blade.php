<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @if (session()->has('user'))
    @include('sweetalert::alert')
    <nav class="navbar navbar-expand-lg bg-body-tertiary" style="margin: 5px; margin-right: 30px">
        <div class="container-fluid">
          <a class="navbar-brand" href="/dashboard">Ecommerce Dashboard</a>
          <ul class="nav justify-content-end">
            <li class="nav-item">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ session('user') }}
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end">
                      <li><a class="dropdown-item" href="/logout">Logout</a></li>
                    </ul>
                  </div>
            </li>
          </ul>
        </div>
      </nav>
      <div class="card" style="margin: 20px">
        <div class="card-header" style="font-size: 20px; display: flex; justify-content: space-between">
            <div>Product</div>
            <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
    +
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Form</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="post" action="/dashboard/product" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label" style="font-size:15px">Images<span style="color: red">*</span></label>
                  <input type="file" name="photo" class="form-control" accept=".jpg,.jpeg,.bmp,.png,.gif,.doc,.docx,.csv,.rtf,.xlsx,.xls,.txt,.pdf,.zip">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label" style="font-size:15px">Title<span style="color: red">*</span></label>
                  <input type="text" name="title" class="form-control" id="exampleInputPassword1">
                </div>
                <label for="exampleInputPassword1" class="form-label" style="font-size:15px">Description<span style="color: red">*</span></label>
                <div class="form-floating">
                    <textarea class="form-control" name="description" id="floatingTextarea2" style="height: 100px"></textarea>
                  </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label" style="font-size:15px">Quantity<span style="color: red">*</span></label>
                    <input type="text" name="quantity" class="form-control" id="exampleInputPassword1">
                    <div  class="form-text" style="font-size: 15px">
                        Must be Number.
                      </div> 
                </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label" style="font-size:15px">Price<span style="color: red">*</span></label>
                    <input type="text" name="price" class="form-control" id="exampleInputPassword1">
                    <div  class="form-text" style="font-size: 15px">
                        Must be Number.
                      </div>  
                </div>
                
             
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
      </div>
    </div>
  </div>
        </div>
        <div class="card-body">
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" >
                <thead>
                  <tr>
                    <th class="th-sm">S.No
                    </th>
                    <th class="th-sm">Image
                    </th>
                    <th class="th-sm">Title
                    </th>
                    <th class="th-sm">Description
                    </th>
                    <th class="th-sm">Quantity
                    </th>
                    <th class="th-sm">Price
                    </th>
                    <th class="th-sm">Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach (session('product') as $product)
                    <tr>
                    <td >{{$product->id}}</td>
                    <td><img src="{{ asset('storage/images/'.$product->image)}}" alt="productimg" width="100px"></td>
                    <td>{{$product->title}}</td>
                    <td style="width: 250px">{{$product->description}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->price}}</td>
                    <td><a  style="margin-left:20px; margin-right: 20px" href="/product/edit/{{$product->id}}"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"  fill="	grey" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                      <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                    </svg></a>
                        <a  href="/product/delete/{{$product->id}}"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="grey" class="bi bi-trash-fill" viewBox="0 0 16 16">
                          <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                        </svg></button></td>
                    </tr>
                    @endforeach
                
                </tbody>
              </table>
        </div>
      </div>
     
    @endif
     
</html>