<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;

class CartController extends Controller
{
    public function index()
    {
      $cart=Cart::all();
      return response()->json([
        'cart'=>$cart
      ]);
    }

    public function create($id,$cid)
    {
        
        $product=Product::where('id',$id)->get();
        // return $product;
        $cart=new Cart();
        $cart->image=$product[0]->image;
        $cart->title=$product[0]->title;
        $cart->description=$product[0]->description;
        $cart->price=$product[0]->price;
        $cart->cid=$cid;
        $cart->save();
        return response()->json([
            'message'=>'Sucessfully added to cart'
        ]);
    }
    public function increasequantity($id,$quantity)
    {
        $quantity=$quantity+1;
        Cart::where('id',$id)->update(['quantity'=>$quantity]);
        return response()->json([
            'message'=>'Sucessfully increase qauntity'
        ]);
    }

    public function decreasequantity($id,$quantity)
    {
        if($quantity<1){
            $quantity=$quantity-1;
            Cart::where('id',$id)->update(['quantity'=>$quantity]);
            return response()->json([
                'message'=>'Sucessfully decrease quantity'
            ]);
        }else{
            Cart::where('id',$id)->delete();
            return response()->json([
                'message'=>'Sucessfully decrease quantity'
            ]);
        }
        
    }

    public function destroy($id)
    {
        Cart::where('id',$id)->delete();
        return response()->json([
            'message'=>'Sucessfully deleted'
        ]);
    }
}
