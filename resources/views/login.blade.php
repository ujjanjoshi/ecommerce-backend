<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  {{session()->forget('user')}}
  @include('sweetalert::alert')
    <section class="vh-100">
        <div class="container py-5 h-100">
          <div class="row d-flex align-items-center justify-content-center h-100">
            <div class="col-md-8 col-lg-7 col-xl-6">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                class="img-fluid" alt="Phone image">
            </div>
            <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
              <form method="post" action="/login">
                @csrf
                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example13">Email address</label>
                  <input type="email" name= "useremail" id="form1Example13" class="form-control form-control-lg" />
                  
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example23">Password</label>
                  <input type="password"  name= "password" id="form1Example23" class="form-control form-control-lg" />
                  
                </div>
      
                <div class="d-flex justify-content-around align-items-center mb-4">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                <div class="text-center text-lg-start mt-4 pt-2">
                    <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="/register"
                        class="link-danger">Register</a></p>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </section>
     
</html>