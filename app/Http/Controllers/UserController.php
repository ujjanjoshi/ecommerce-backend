<?php

namespace App\Http\Controllers;
session_start();
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
class UserController extends Controller
{
    public function register(Request $request){
       
        $username=$request->get('username');
        $useremail=$request->get('useremail');
        $password=$request->get('password');
        $confrimpassword=$request->get('password_confirmation');
        $role=$request->get('role');
        $user = User::where('email',$request->get('useremail'))->first();
        if($role=="Admin"){
            if ( count((array)$user)!= 0) {
                Alert::warning('Register Error!','User Already Exist');
                return redirect()->back();
        } else {
            $exp = "/@gmail.com/i";
            if($username=='' || $useremail=='' || $password=='' || $confrimpassword==''){
                Alert::error('Invalid Data!','Please fill the form as required');
                return redirect()->back();
            } else if (preg_match($exp, $useremail) != 1) {
                Alert::error('Invalid Data!','Please fill the form as required');
                return redirect()->back();
            }elseif(strlen($password)<8){
                Alert::error('Invalid Data!','Please fill the form as required');
                return redirect()->back();
            }
            elseif($password!=$confrimpassword){
                Alert::error('Invalid Data!','Password doesnot Match');
                return redirect()->back();
            }
            else{
                $user=new User();
                $user->name=$username;
                $user->email=$useremail;
                $user->password=$password;
                $user->role=$role;
                $user->save();
                Alert::success('Registered Succesfully!!','Please Login');
                return view('login');
            }
        }
        }else{
            if ( count((array)$user)!= 0) {
                return response()->json([
                    "message"=>'User Already Exist'
                ]);;
            }else{
                $validator=Validator::make($request->all(),[
                    'username'=>'required|string',
                    'useremail'=>'required|string',
                    'password'=>'required',
                    'password_confirmation'=>'required',
                    'role'=>'required'
                ]);
                if($validator->fails()){
                    return response()->json([
                        "message"=>'Invalid Data Please Fill the Form Properly'
                    ]);; 
                }elseif(strlen($password)<8){
                    return response()->json([
                        "message"=>'Invalid Data Please Fill the Form Properly'
                    ]);; 
                }
                elseif($password!=$confrimpassword){
                    return response()->json([
                        "message"=>'Password doesnt Match'
                    ]);; 
                }else{
                    $user=new User();
                    $user->name=$username;
                    $user->email=$useremail;
                    $user->password=$password;
                    $user->role=$role;
                    $user->save();
                    return response()->json([
                        "message"=>"Registered Sucessful"
                    ]);
                }
            
            }
        }
     
    }
    public function login(Request $request){
       
        $useremail=$request->get('useremail');
        $password=$request->get('password');
        if($useremail=="" || $password==""){
            Alert::warning('Login Error!','Invalid credentials');
            return redirect()->back();
        }
        $user = User::where('email',$request->get('useremail'))->first();
        if ( count((array)$user)!= 0) {
            if($user->password==$password){
                session(['user'=>ucfirst($user->name)]);  
                Alert::success('Login Succesfully!!','Welcome to Dashboard');
                return redirect('/dashboard');
            }else{
                Alert::warning('Login Error!','Invalid credentials');
                return redirect()->back();
            }
        } else{
            Alert::warning('Login Error!','User Not Registered');
                return redirect()->back();
        }
       
    }

    public function logincustomer(Request $request){
        $validator=Validator::make($request->all(),[
            'useremail'=>'required|string',
            'password'=>'required',
        ]);
        if($validator->fails()){
            return response()->json([
                "message"=>'Invalid Data '
            ]);; 
        }
        $user = User::where('email',$request->get('useremail'))->first();
        if ( count((array)$user)!= 0) {
            if($user->password==$request->get('password')){

                return response()->json([
                    "message"=>'Login Successful '
                ]);; 
            }else{
                return response()->json([
                    "message"=>'Unauthorized '
                ]);; 
            }
        } else{
            return response()->json([
                "message"=>'Unauthorized '
            ]);; 
        }
    }
    public function logout(){
        session()->flush();
        Alert::success('Logout Sucessfully!!','Visit Again');
        return redirect('/');

    }
}
