<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    public function storeproduct(Request $request){
        $validator=Validator::make($request->all(),[
            'title'=>'required|string',
            'description'=>'required|string',
            'quantity'=>'required|numeric',
            'price'=>'required|numeric',
            'photo' => 'required'
        ]);
        if ($validator->fails()) {
            Alert::error('Invalid Data!','Please fill the form as required');
           return redirect('/dashboard');
        }
        $product = Product::where('title',$request->get('title'))->first();
        if ( count((array)$product)!= 0) {
                Alert::warning('Product Info Alert!','Product Already Exist');
                return redirect()->back();
        } else {
        $filename= time().'.'.$request->file('photo')->getClientOriginalExtension(); 
        $request->file('photo')->storeAs('public/images/',$filename);
        $product=new Product();
        $product->image=$filename;
        $product->title= $request->get('title');
        $product->description=$request->get('description');
        $product->quantity=$request->get('quantity');
        $product->price=$request->get('price');
        $product->save();
        Alert::success('Product Added!!','Process Sucessful');
        $products=Product::all();
        session(['product'=>$products]);
        return redirect('/dashboard');
    }
    }

    public function editproductview($id){
        $product=Product::where('id',$id)->first();
        return view('editproduct')->with('products',$product);
    }
    public function updateproduct(Request $request,$id){
        if($request->hasFile('photo')==1){
            $filename= time().'.'.$request->file('photo')->getClientOriginalExtension(); 
            $request->file('photo')->storeAs('public/images/',$filename);
            Product::where('id',$id)->update([
                'image'=>$filename,
                'title'=>$request->get('title'),
                'description'=>$request->get('description'),
                'quantity'=>$request->get('quantity'),
                'price'=>$request->get('price')
            ]);
            Alert::success('Product Updated!!','Process Sucessful');
            $products=Product::orderBy('id','ASC')->get();
            session(['product'=>$products]);
            return redirect('/dashboard');
           
        }else{
            Product::where('id',$id)->update([
                'title'=>$request->get('title'),
                'description'=>$request->get('description'),
                'quantity'=>$request->get('quantity'),
                'price'=>$request->get('price')
            ]);
            Alert::success('Product Updated!!','Process Sucessful');
            $products=Product::orderBy('id','asc')->get();
            session(['product'=>$products]);
            return redirect('dashboard'); 
        }
       
    }

    public function deleteproduct($id){
        Product::where('id',$id)->delete();
        Alert::success('Product Deleted!!','Process Sucessful');
        $products=Product::orderBy('id','ASC')->get();
        session(['product'=>$products]);
        return redirect('/dashboard');
    }

    public function viewproduct($id){
        
            $product=Product::where('id',$id)->get();
            return response()->json(
                [
                    'product'=>$product
                ]
                );
    }

    public function cartproducttemporary(Request $request){
        $ids = $request;
        $product=Product::whereIN('id',$ids)->get();
        return $product;
    }
    public function viewallproduct(){
            $product=Product::orderBy('id','ASC')->get();
            return response()->json(
                [
                    'product'=>$product
                ]
                );
        }

        public function getImage($path)
        {
            // $image = Storage::get('public/storage/images/'.$path);
            // return 'images/'.$path;
            $image = Storage::disk('public')->get('images/'.$path); 
            // return Storage::response($image);
            return response($image, 200)->header('Content-Type', 'image/jpeg');
            // return response($image, 200)->header('Content-Type', Storage::getMimeType($path));
        }
}